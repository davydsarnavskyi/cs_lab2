def convertToBinary(n):
	if (int(n) >= 0):
		return positivenum(int(n))
	else:
		return twoscompliment(int(n))


def positivenum(n):  # converting a postitive number to binary number
	p = list(bin(int(n)).replace("0b", ""))
	p.insert(0, '0')
	return "".join(p)


def twoscompliment(n):  # returning two's compliment of a number in form of list
	arr = []
	n = n * -1
	for i in positivenum(n):
		if i == '1':
			arr.append('0')
		elif i == '0':
			arr.append('1')
	temp = arr
	temp.reverse()

	if (temp[0] == '0'):
		temp[0] = '1'
	else:
		temp[0] = '0'
		for j in range(1, len(arr)):
			if (temp[j] == '0'):
				temp[j] = '1'
				break
			else:
				temp[j] = '0'
	temp.reverse()
	return "".join(temp)


def shiftToRight(shift):  # right shifting
	s = shift[0]
	s1 = shift[:-1]
	s1 = "".join(s1)
	s2 = s + s1
	return s2


def adding(b1, b2):  # adding two binary  numbers
	add = []
	carry = 0
	for x in range(len(b1) - 1, -1, -1):
		if b1[x] == '1' and b2[x] == '1':
			if carry == 1:
				add.append('1')
			else:
				add.append('0')
				carry = 1
		elif b1[x] == '0' and b2[x] == '0':
			if carry == 1:
				add.append('1')
				carry = 0
			else:
				add.append('0')
		elif (b1[x] == '0' and b2[x] == '1') or (b1[x] == '1' and b2[x] == '0'):
			if carry == 1:
				add.append('0')
			else:
				add.append('1')
	add.reverse()
	return "".join(add)


def btoD(n):
	print()
	print("The answer is:", end=" ")
	c = int(n, 2)
	c = c * -1
	print(str(c))


def binaryToDecimal(n):
	if (n[0] == '0'):
		print()
		print("The Answer is:", end=" ")
		print(int(n, 2))
	elif (n[0] == '1'):
		arr = []
		for i in range(len(n)):
			if n[i] == '1':
				arr.append('0')
			elif n[i] == '0':
				arr.append('1')
		temp = arr
		temp.reverse()

		if (temp[0] == '0'):
			temp[0] = '1'
		else:
			temp[0] = '0'
			for j in range(1, len(arr)):
				if (temp[j] == '0'):
					temp[j] = '1'
					break
				else:
					temp[j] = '0'
		temp.reverse()
		btoD("".join(temp))


def booth(b1, b2):
	m = int(b1)
	l1 = convertToBinary(b1)
	r = int(b2)
	m1 = m * -1
	if (m >= 0):
		l = convertToBinary(m)
		x = len(l)
		A = list(convertToBinary(m))
		A.insert(0, '0')
		S = list(convertToBinary(m1))
		S.insert(0, '1')
	else:
		p = m * -1
		l = convertToBinary(p)
		x = len(l)
		A = list(convertToBinary(m))
		A.insert(0, '1')
		S = list(convertToBinary(m1))
		S.insert(0, '0')
	if (r >= 0):
		l = convertToBinary(r)
		y = len(l)
		P = list(convertToBinary(r))
	else:
		r = r * -1
		l = convertToBinary(r)
		y = len(l)
		P = list(convertToBinary(r * -1))
	for i in range(y + 1):
		A.append('0')
		S.append('0')
	for j in range(0, x):
		P.insert(j, '0')

	P.insert(0, '0')
	P.append('0')

	count = 0
	while (count != y):
		qneg1 = P[len(P) - 1]
		q1 = P[len(P) - 2]
		if (q1 == '0' and qneg1 == '1'):
			s = adding(P, A)
			print("P=" + str("".join(P)), end=" ")
			print("the last two bits are " + str(P[len(P) - 2]) + str(P[len(P) - 1]))
			print(str("".join(P)) + "." + "P=P+S")
			P = shiftToRight(s)
			print(str("".join(P)) + "." + "Right Shift")
		if (q1 == '1' and qneg1 == '0'):
			s = adding(P, S)
			print("P=" + str("".join(P)), end=" ")
			print("the last two bits are " + str(P[len(P) - 2]) + str(P[len(P) - 1]))
			print(str("".join(P)) + "." + "P=P+S")
			P = shiftToRight(s)
			print(str("".join(P)) + "." + "Right Shift")

		if ((q1 == '0' and qneg1 == '0') or (q1 == '1' and qneg1 == '1')):
			print("P=" + str("".join(P)), end=" ")
			print("the last two bits are " + str(P[len(P) - 2]) + str(P[len(P) - 1]))
			print("After right shift: ", end=" ")
			P = shiftToRight(P)
			print(str("".join(P)))
		count = count + 1

	binaryToDecimal(P[1:len(P) - 1])

def divide_in_column(dividend, divisor):
    # Перевірка на випадок ділення на нуль
    if divisor == 0:
        raise ValueError("Divisor cannot be zero.")

    # Ініціалізація змінних
    quotient = 0  # Частка
    remainder = 0  # Залишок

    # Проходження через кожен біт ділення
    for i in range(len(bin(dividend))-2):
        # Зсув вправо дільника та порівняння з діленцем
        remainder <<= 1
        remainder |= (dividend >> (len(bin(dividend))-3-i)) & 1

        # Якщо дільник менший за залишок, віднімаємо та встановлюємо біт частки в 1
        if divisor <= remainder:
            remainder -= divisor
            quotient |= 1

        # Зсув вліво частки
        quotient <<= 1

    # Видаляємо непотрібний останній зсув
    quotient >>= 1

    # Повертаємо частку та залишок
    return quotient, remainder

def divide_in_column(dividend, divisor):
    if divisor == 0:
        raise ValueError("Divisor cannot be zero.")

    quotient = 0
    remainder = 0

    for i in range(len(bin(dividend))-2):
        remainder <<= 1
        remainder |= (dividend >> (len(bin(dividend))-3-i)) & 1

        if divisor <= remainder:
            remainder -= divisor
            quotient |= 1

        quotient <<= 1

    quotient >>= 1

    return quotient, remainder

import struct

def float_to_ieee754(number):
    # Перетворення десяткового числа в формат IEEE 754
    packed = struct.pack('!f', number)
    ieee754 = struct.unpack('!I', packed)[0]
    return ieee754

def ieee754_to_float(ieee754):
    # Перетворення числа у форматі IEEE 754 на десяткове число
    packed = struct.pack('!I', ieee754)
    number = struct.unpack('!f', packed)[0]
    return number

def multiply_ieee754(x, y):
	# Перетворення десяткових чисел в формат IEEE 754
	x = float_to_ieee754(x)
	y = float_to_ieee754(y)

	# Розпаковка значень операндів
	sign_x = (x >> 31) & 1
	exponent_x = (x >> 23) & 0xFF
	mantissa_x = x & 0x7FFFFF

	sign_y = (y >> 31) & 1
	exponent_y = (y >> 23) & 0xFF
	mantissa_y = y & 0x7FFFFF

	# Перевірка на спеціальні значення
	if exponent_x == 0 or exponent_y == 0:
		if exponent_x == 0 and mantissa_x == 0:
			# x = 0, результат = 0
			return 0
		elif exponent_y == 0 and mantissa_y == 0:
			# y = 0, результат = 0
			return 0
		else:
			# Хоча б один з операндів - нескінченність, результат = нескінченність
			return float('inf')

	# Обчислення знаку результату
	sign_result = sign_x ^ sign_y

	# Обчислення мантиси результату
	mantissa_result = (mantissa_x + 0x800000) * (mantissa_y + 0x800000)
	mantissa_result >>= 23

	# Обчислення експоненти результату
	exponent_result = exponent_x + exponent_y - 127

	# Нормалізація мантиси та експоненти
	if mantissa_result & 0x800000:
		mantissa_result >>= 1
		exponent_result += 1

	# Перевірка на переповнення
	if exponent_result >= 255:
		# Результат перевищує максимальне значення, встановлюємо результат в нескінченність
		return float('inf')
	elif exponent_result <= 0:
		# Результат менше або дорівнює нулю, встановлюємо результат в 0
		return 0

	# Упаковка результату в IEEE 754 формат
	result = (sign_result << 31) | (exponent_result << 23) | mantissa_result
	return result

if __name__ == '__main__':
	num1 = 7.252
	num2 = 3.612
	print(f"результат множення IEEE 754 Floating Point IEEE 754 Floating Point  {num1} на {num2} = {ieee754_to_float(multiply_ieee754(num1, num2))}")
	#print(f"результат ділення {num1} на {num2} в стовпчик = {divide_in_column(num1, num2)}")
	#booth(num1, num2)

